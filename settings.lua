
-- Copyright (C) 2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

if mods['Rampant'] then
	data.raw['int-setting']['rampant--maxNumberOfBuilders'].maximum_value = nil
	data.raw['int-setting']['rampant--maxNumberOfBuilders'].default_value = 120
	data.raw['int-setting']['rampant--maxNumberOfBuilders'].localised_description = {'mod-setting-description.rampant--maxNumberOfBuilders_compressed_biter_squads', {'mod-setting-description.rampant--maxNumberOfBuilders'}}

	data.raw['int-setting']['rampant--maxNumberOfSquads'].maximum_value = nil
	data.raw['int-setting']['rampant--maxNumberOfSquads'].default_value = 240
	data.raw['int-setting']['rampant--maxNumberOfSquads'].localised_description = {'mod-setting-description.rampant--maxNumberOfSquads_compressed_biter_squads', {'mod-setting-description.rampant--maxNumberOfSquads'}}
end

data:extend({
	{
		type = 'bool-setting',
		name = 'compressed-biter-squads-enable',
		setting_type = 'runtime-global',
		default_value = true,
		order = 'a',
	},

	{
		type = 'bool-setting',
		name = 'compressed-biter-squads-right-after',
		setting_type = 'runtime-global',
		default_value = false,
		order = 'b',
	},

	{
		type = 'bool-setting',
		name = 'compressed-biter-squads-only-enemy',
		setting_type = 'runtime-global',
		default_value = true,
		order = 'c',
	},
})
