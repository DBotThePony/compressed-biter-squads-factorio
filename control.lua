
-- Copyright (C) 2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local compressed_biters
local compressed_squads
local delayed_entity_create_list
local delayed_entity_create_list2
local watched_groups

local MAX_TRACKED_POSITIONS = 24
local PREFERED_DISTANCE = 20
local REMEMBER_DISTRACTION_FOR = 6 * 60
local TRASH_SQUAD_AFTER = 30 * 60 -- 30 seconds

local on_unit_added_to_group
local ENABLED, RIGHT_AFTER, ONLY_ENEMY = true, true, true

local function report_error(str)
	log('Reporting managed error: ' .. str)
end

local function asserted_valid(funcname, ...)
	local success, reported = pcall(...)

	if not success then
		report_error('GAME ENGINE BUG: LuaEntity.valid returned true, ' .. funcname .. ' reported ' .. tostring(reported))
	end

	return success, reported
end

script.on_init(function()
	global.compressed_biters = {}
	global.compressed_squads = {}
	global.delayed_entity_create_list = {}
	global.delayed_entity_create_list2 = {}
	global.watched_groups = {}

	compressed_biters = global.compressed_biters
	compressed_squads = global.compressed_squads
	delayed_entity_create_list = global.delayed_entity_create_list
	delayed_entity_create_list2 = global.delayed_entity_create_list2
	watched_groups = global.watched_groups

	ENABLED = settings.global['compressed-biter-squads-enable'].value
	RIGHT_AFTER = settings.global['compressed-biter-squads-right-after'].value
	ONLY_ENEMY = settings.global['compressed-biter-squads-only-enemy'].value
end)

script.on_load(function()
	compressed_biters = global.compressed_biters
	compressed_squads = global.compressed_squads
	delayed_entity_create_list = global.delayed_entity_create_list
	delayed_entity_create_list2 = global.delayed_entity_create_list2
	watched_groups = global.watched_groups

	ENABLED = settings.global['compressed-biter-squads-enable'].value
	RIGHT_AFTER = settings.global['compressed-biter-squads-right-after'].value
	ONLY_ENEMY = settings.global['compressed-biter-squads-only-enemy'].value
end)

script.on_configuration_changed(function()
	local removed = {}

	for _, group_data in pairs(compressed_squads) do
		::LOOP::

		for name in pairs(group_data.members) do
			if not game.entity_prototypes[name] then
				group_data.members[name] = nil

				if not removed[name] then
					removed[name] = true
					log('Removing data about unknown unit type ' .. tostring(name))
				end

				goto LOOP
			end
		end
	end
end)

script.on_event(defines.events.on_runtime_mod_setting_changed, function()
	ENABLED = settings.global['compressed-biter-squads-enable'].value
	RIGHT_AFTER = settings.global['compressed-biter-squads-right-after'].value
	ONLY_ENEMY = settings.global['compressed-biter-squads-only-enemy'].value
end)

local spawn_position_cache = {}
local next_ring = 0

local math_sqrt = math.sqrt

local function ring_sorter(a, b)
	local len_a = a.x * a.x + a.y * a.y
	local len_b = b.x * b.x + b.y * b.y

	if len_a == len_b then
		return a.index < b.index -- to avoid undetermined behavior of quicksort with equal values
	else
		return len_a < len_b
	end
end

local function make_ring()
	if next_ring == 0 then
		next_ring = 1
		table.insert(spawn_position_cache, {x = 0, y = 0})
		return
	end

	local generated = {}
	local index = 1

	for x = -next_ring, next_ring do
		-- верхняя строка
		generated[index] = {x = x, y = -next_ring, index = index}
		index = index + 1

		-- нижняя строка
		generated[index] = {x = x, y = next_ring, index = index}
		index = index + 1
	end

	for y = -next_ring + 1, next_ring - 1 do
		-- левая стенка строка
		generated[index] = {x = -next_ring, y = y, index = index}
		index = index + 1

		-- правая строка
		generated[index] = {x = next_ring, y = y, index = index}
		index = index + 1
	end

	table.sort(generated, ring_sorter)
	index = #spawn_position_cache

	for i = 1, #generated do
		spawn_position_cache[i + index] = generated[i]
	end

	next_ring = next_ring + 1
end

local function get_spawn_ring(amount)
	if #spawn_position_cache >= amount then return spawn_position_cache end

	while #spawn_position_cache < amount do
		make_ring()
	end

	return spawn_position_cache
end

local function distance(a, b)
	return math_sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))
end

local function vector_add(a, b)
	return {x = a.x + b.x, y = a.y + b.y}
end

local function decompress(group_data)
	if group_data.decompressed then return end

	group_data.decompressed = true
	local group = group_data.self
	local valid = group.valid
	local surface = group_data.surface

	if not surface.valid then
		return
	end

	local spawn_pos = valid and group.position or group_data.position_2 or {x = 0, y = 0}
	local squad_pos = valid and group.position or group_data.position_2 or {x = 0, y = 0}
	local determined_distance = math.abs(distance(spawn_pos, squad_pos) - PREFERED_DISTANCE)

	if group_data.position_2 then
		local element = group_data.position_2

		while element.prev do
			local new_distance = distance(element, squad_pos)

			if math.abs(new_distance - PREFERED_DISTANCE) < determined_distance then
				determined_distance = math.abs(new_distance - PREFERED_DISTANCE)
				spawn_pos = element

				if determined_distance < 1 then
					break
				end
			end

			element = element.prev
		end
	end

	local force = group_data.force

	local determine = 0

	for name, data in pairs(group_data.members) do
		determine = determine + #data.members
	end

	local points = get_spawn_ring(determine)
	local index = 1

	for name, data in pairs(group_data.members) do
		compressed_biters[data.unit_number] = nil

		if data.numbers and rendering.is_valid(data.numbers) then
			rendering.destroy(data.numbers)
			data.numbers = nil
		end

		for i, member in ipairs(data.members) do
			local cdata = {
				name = name,
				position = vector_add(spawn_pos, points[index]),
				force = force,
				surface = surface,
				health = member.health,
				--[[command = data.self.valid and (data.self.command) or
					group_data.self.valid and (group_data.self.command) or group_data.last_command or nil,]]

				set_group_command = group_data.self.valid and group_data.self.command and true,
				-- group = group.valid and group or nil,
			}

			if group_data.self.valid and group_data.self.command then
				cdata.command = group_data.self.command
			elseif data.self.valid and data.self.command then
				cdata.command = data.self.command
			elseif group_data.last_command then
				cdata.command = group_data.last_command
			else
				cdata.command = {
					type = defines.command.attack_area,
					destination = squad_pos,
					radius = 8
				}
			end

			index = index + 1
			table.insert(delayed_entity_create_list, cdata)

			if valid then
				cdata.group = group
			end
		end
	end
end

local valid_commands_for_compression = {
	[defines.command.attack] = true,
	[defines.command.go_to_location] = true,
	[defines.command.attack_area] = true,
}

script.on_event(defines.events.on_tick, function(event)
	local tick = event.tick

	for iteration = 1, 4 do
		local key, group = next(watched_groups, global.watched_groups_key)

		if group then
			if not group.valid or compressed_squads[key] then
				watched_groups[key] = nil
				key = nil
			else
				local command = group.command and group.command.type

				if valid_commands_for_compression[command] then
					if ENABLED then
						local group_data

						for i, unit in ipairs(group.members) do
							group_data = on_unit_added_to_group({tick = tick, group = group, unit = unit}, group_data)
						end
					end

					watched_groups[key] = nil
					key = nil
				end
			end
		end

		::CONTINUE::
		global.watched_groups_key = key
	end

	for iteration = 1, 4 do
		local key, group_data = next(compressed_squads, global.compressed_squads_key)

		if group_data then
			if not group_data.self.valid then
				decompress(group_data)
				compressed_squads[key] = nil
				key = nil
				goto CONTINUE
			end

			if not group_data.decompressed then
				local new = group_data.self.position

				if group_data.self.command then
					group_data.last_command = group_data.self.command

					if group_data.self.command.type == defines.command.build_base or group_data.self.command.type == defines.command.compound then
						decompress(group_data)
						goto CONTINUE
					end
				end

				if distance(new, group_data.position_2) >= 1.5 then
					group_data.position_2.next = new
					new.prev = group_data.position_2
					group_data.position_2 = new
					group_data.nothing_happens = nil

					if group_data.position_count >= MAX_TRACKED_POSITIONS then
						group_data.position_1 = group_data.position_1.next
						group_data.position_1.prev = nil
					else
						group_data.position_count = group_data.position_count + 1
					end
				else
					group_data.nothing_happens = group_data.nothing_happens or tick
				end

				if group_data.self.distraction_command then
					group_data.last_distraction_command = group_data.self.distraction_command
					group_data.last_distraction_command.tick = tick
				elseif group_data.last_distraction_command and group_data.last_distraction_command.tick + REMEMBER_DISTRACTION_FOR <= tick then
					group_data.last_distraction_command = nil
				end
			else
				local new = group_data.self.position

				if distance(new, group_data.position_2) >= 1.5 then
					group_data.position_2.next = new
					new.prev = group_data.position_2
					group_data.position_2 = new
					group_data.nothing_happens = nil

					if group_data.position_count >= MAX_TRACKED_POSITIONS then
						group_data.position_1 = group_data.position_1.next
						group_data.position_1.prev = nil
					else
						group_data.position_count = group_data.position_count + 1
					end
				else
					group_data.nothing_happens = group_data.nothing_happens or tick

					if group_data.nothing_happens + TRASH_SQUAD_AFTER <= tick and (not group_data.self.command or (group_data.self.command.type ~= defines.command.build_base and group_data.self.command.type ~= defines.command.compound)) then
						--[[if group_data.self.is_script_driven then
							group_data.self.set_autonomous()
							group_data.self.start_moving()
						end]]

						group_data.self.set_command({
							type = defines.command.attack_area,
							destination = new,
							radius = 128
						})

						group_data.nothing_happens = tick
					end
				end
			end

			::CONTINUE::
		else
			global.compressed_squads_key = key
			break
		end

		global.compressed_squads_key = key
	end

	for i = #delayed_entity_create_list2, 1, -1 do
		local data = delayed_entity_create_list2[i]

		if data.self.valid then
			local created = data.self

			if data.set_group_command and data.group and data.group.valid then
				asserted_valid('LuaGroup.add_member', data.group.add_member, created)
				asserted_valid('LuaEntity.set_command', created.set_command, {type = defines.command.group, group = data.group})
			elseif data.command and (data.command.type == defines.command.attack_area or data.command.type == defines.command.go_to_location or data.command.type == defines.command.flee) then
				data.command.destination = created.surface.find_non_colliding_position(created.name, data.command.destination, 0, 1, false)

				if data.group and data.group.valid then
					asserted_valid('LuaGroup.add_member', data.group.add_member, created)
				end

				asserted_valid('LuaEntity.set_command', created.set_command, data.command)
			end
		end

		delayed_entity_create_list2[i] = nil
	end

	for i = #delayed_entity_create_list, 1, -1 do
		local data = delayed_entity_create_list[i]

		local pos = data.surface.find_non_colliding_position(data.name, {x = data.position.x, y = data.position.y}, 0, 1, false)

		local created = data.surface.create_entity({
			name = data.name,
			position = pos,
			force = data.force,
		})

		if created and created.valid then
			if data.health then
				created.health = data.health
			end

			--[[if data.group and data.group.valid then
				data.group.add_member(created)
			end]]

			data.self = created
			table.insert(delayed_entity_create_list2, data)
		end

		delayed_entity_create_list[i] = nil
	end
end)

function on_unit_added_to_group(event, group_data)
	local group = event.group

	group_data = group_data or compressed_squads[group.group_number]

	if not group_data then
		group_data = {
			self = group,
			force = group.force,
			surface = group.surface,
			group_number = group.group_number,
			members = {},
			position_1 = group.position,
			position_2 = group.position,
			position_count = 1,
			decompressed = false
		}

		group_data.position_1.next = group_data.position_2
		group_data.position_2.prev = group_data.position_1

		compressed_squads[group.group_number] = group_data
	elseif group_data.decompressed then
		return group_data
	end

	local unit = event.unit
	local name = event.unit.name
	local find_compressed = group_data.members[name]

	if not find_compressed then
		for _, member in ipairs(event.group.members) do
			if member ~= unit and member.name == name then
				find_compressed = member
				break
			end
		end

		if not find_compressed then
			find_compressed = unit
		end

		local data = {
			self = find_compressed,
			unit_number = find_compressed.unit_number,
			group = group.group_number,
			members = {}
		}

		group_data.members[name] = data
		compressed_biters[find_compressed.unit_number] = data

		return group_data
	elseif find_compressed.self == unit then
		return group_data
	end

	table.insert(find_compressed.members, {
		health = unit.health
	})

	if (not find_compressed.numbers or not rendering.is_valid(find_compressed.numbers)) and find_compressed.self.valid then
		local success, numbers = asserted_valid('rendering.draw_text', rendering.draw_text, {
			text = '0',
			surface = event.group.surface,
			target = find_compressed.self,
			color = {r = 1, g = 1, b = 1, a = 1},
			scale = 2,
			target_offset = {x = 0, y = -4},
			scale_with_zoom = true,
			alignment = 'center',
			draw_on_ground = false,
		})

		if success then
			find_compressed.numbers = numbers
		end
	elseif not find_compressed.self.valid or find_compressed.numbers and not rendering.is_valid(find_compressed.numbers) then
		find_compressed.numbers = nil
	end

	if find_compressed.numbers then
		rendering.set_text(find_compressed.numbers, tostring(#find_compressed.members))
	end

	unit.destroy()

	return group_data
end

script.on_event(defines.events.on_unit_added_to_group, function(event)
	if not ENABLED then return end

	local group = event.group
	if not group.valid then return end -- other mod removed the group right after
	-- such as rampant

	--if group.is_script_driven then return end
	if ONLY_ENEMY and group.force.name ~= 'enemy' then return end

	if RIGHT_AFTER then
		on_unit_added_to_group(event)
	elseif not compressed_squads[group.group_number] then
		watched_groups[group.group_number] = group
	end
end)

script.on_event(defines.events.on_entity_damaged, function(event)
	local unit = event.entity

	if unit.type == 'unit' then
		if unit.unit_group and unit.unit_group.valid and compressed_squads[unit.unit_group.group_number] then
			decompress(compressed_squads[unit.unit_group.group_number])
		end
	elseif event.cause and event.cause.type == 'unit' then
		unit = event.cause

		if unit.unit_group and unit.unit_group.valid and compressed_squads[unit.unit_group.group_number] then
			decompress(compressed_squads[unit.unit_group.group_number])
		end
	end
end)

script.on_event(defines.events.on_entity_died, function(event)
	local unit = event.entity

	if unit.type == 'unit' then
		if unit.unit_group and unit.unit_group.valid and compressed_squads[unit.unit_group.group_number] then
			decompress(compressed_squads[unit.unit_group.group_number])
		end
	end
end)
